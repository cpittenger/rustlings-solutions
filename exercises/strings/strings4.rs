/*
This file is part of rustlings-solutions.

rustlings-solutions is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

rustlings-solutions is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with rustlings-solutions. If not, see <https://www.gnu.org/licenses/>.
*/

// strings4.rs

// Ok, here are a bunch of values-- some are `String`s, some are `&str`s. Your
// task is to call one of these two functions on each value depending on what
// you think each value is. That is, add either `string_slice` or `string`
// before the parentheses on each line. If you're right, it will compile!
// No hints this time!

fn string_slice(arg: &str) {
    println!("{arg}");
}
fn string(arg: String) {
    println!("{arg}");
}

fn main() {
    string_slice("blue");
    // string("red".to_string());
    string("red".to_owned());
    // string(String::from("hi"));
    string("hi".to_owned());
    string("rust is fun!".to_owned());
    // string_slice("nice weather".into());
    string_slice("nice weather");
    // string(format!("Interpolation {}", "Station"));
    string("Interpolation Station".to_owned());
    // string_slice(&String::from("abc")[0..1]);
    string_slice(&"abc"[..1]);
    string_slice("  hello there ".trim());
    // string("Happy Monday!".to_string().replace("Mon", "Tues"));
    string("Happy Monday!".replace("Mon", "Tues"));
    string("mY sHiFt KeY iS sTiCkY".to_lowercase());
}

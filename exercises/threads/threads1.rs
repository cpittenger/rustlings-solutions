/*
This file is part of rustlings-solutions.

rustlings-solutions is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

rustlings-solutions is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with rustlings-solutions. If not, see <https://www.gnu.org/licenses/>.
*/

// threads1.rs
// Execute `rustlings hint threads1` or use the `hint` watch subcommand for a hint.
// This program should wait until all the spawned threads have finished before exiting.

use std::thread;
use std::time::Duration;

fn main() {
    // let mut handles = vec![];
    let mut handles = Vec::with_capacity(10);
    for i in 0..10 {
        handles.push(thread::spawn(move || {
            thread::sleep(Duration::from_millis(250));
            println!("thread {i} is complete");
        }));
    }

    let mut completed_threads = 0;
    for handle in handles {
        // TODO: a struct is returned from thread::spawn, can you use it?
        handle
            .join()
            .expect("Could not join on the associated thread");
        completed_threads += 1;
    }

    // if completed_threads != 10 {
    //     panic!("Oh no! All the spawned threads did not finish!");
    // }
    assert_eq!(
        completed_threads, 10,
        "Oh no! All the spawned threads did not finish!"
    );
}

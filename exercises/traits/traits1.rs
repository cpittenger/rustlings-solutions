/*
This file is part of rustlings-solutions.

rustlings-solutions is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

rustlings-solutions is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with rustlings-solutions. If not, see <https://www.gnu.org/licenses/>.
*/

// traits1.rs
// Time to implement some traits!
//
// Your task is to implement the trait
// `AppendBar` for the type `String`.
//
// The trait AppendBar has only one function,
// which appends "Bar" to any object
// implementing this trait.
// Execute `rustlings hint traits1` or use the `hint` watch subcommand for a hint.

trait AppendBar {
    fn append_bar(self) -> Self;
}

impl AppendBar for String {
    // TODO: Implement `AppendBar` for type `String`.
    fn append_bar(self) -> Self {
        // format!("{self}Bar")
        self + "Bar"
    }
}

fn main() {
    // let s = String::from("Foo");
    let s = "Foo".to_owned();
    let s = s.append_bar();
    println!("s: {s}");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_foo_bar() {
        // assert_eq!(String::from("Foo").append_bar(), String::from("FooBar"));
        assert_eq!("Foo".to_owned().append_bar(), "FooBar");
    }

    #[test]
    fn is_bar_bar() {
        assert_eq!(
            // String::from("").append_bar().append_bar(),
            // String::from("BarBar")
            String::new().append_bar().append_bar(),
            "BarBar"
        );
    }
}

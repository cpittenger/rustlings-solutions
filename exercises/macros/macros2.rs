/*
This file is part of rustlings-solutions.

rustlings-solutions is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

rustlings-solutions is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with rustlings-solutions. If not, see <https://www.gnu.org/licenses/>.
*/

// macros2.rs
// Execute `rustlings hint macros2` or use the `hint` watch subcommand for a hint.

fn main() {
    my_macro!();
}

#[macro_export]
macro_rules! my_macro {
    () => {
        println!("Check out my macro!");
    };
}

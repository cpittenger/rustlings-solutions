/*
This file is part of rustlings-solutions.

rustlings-solutions is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

rustlings-solutions is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with rustlings-solutions. If not, see <https://www.gnu.org/licenses/>.
*/

// move_semantics6.rs
// Execute `rustlings hint move_semantics6` or use the `hint` watch subcommand for a hint.
// You can't change anything except adding or removing references.

fn main() {
    // let data = "Rust is great!".to_string();
    let data = "Rust is great!".to_owned();

    get_char(&data);

    string_uppercase(data);
}

// Should not take ownership
// fn get_char(data: &String) -> char {
fn get_char(data: &str) -> char {
    data.chars().last().unwrap()
}

// Should take ownership
// fn string_uppercase(mut data: String) {
//     data = data.to_uppercase();

//     println!("{data}");
// }
fn string_uppercase(data: String) {
    println!("{}", data.to_uppercase());
}
